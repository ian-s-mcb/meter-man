# Meter-Man
A Ruby on Rails web app that helps people record and understand their home electricity usage.

## [Demo][demo]

## Screenshots

![Screenshot 1][screenshot_1]
![Screenshot 2][screenshot_2]
![Screenshot 3][screenshot_3]


## Technologies Used
* [Ruby on Rails][rails]
  * [Active Record][active-record] - for object-relational modeling
  * [Devise][devise] - for authentication
  * [Will Paginate][paginate] - for model pagination
* [PostgreSQL][psql]
* [Heroku][heroku]
* [jQuery][jquery]
* [Twitter Bootstrap][bootstrap]
* [Font Awesome][fontawesome]
* [D3.js][d3]
* [Pivotal Tracker][pivotal-tracker] - for Agile/SCRUM project management

## Installation
See [this document][install] for instructions about installing Meter-Man.

## Contributors
* [Bjorn Johnson][gh-bjorn]
* [Ian S. McBride][gh-ian]
* [Simon Santos][gh-simon]
* [Steven Vargas][gh-steven]

[screenshot_1]: http://i.imgur.com/It8HTLi.png
[screenshot_2]: http://i.imgur.com/xKTUUfF.png
[screenshot_3]: http://i.imgur.com/EQVD3UN.png
[rails]: https://en.wikipedia.org/wiki/Ruby_on_Rails
[active-record]: http://guides.rubyonrails.org/active_record_basics.html
[devise]: http://devise.plataformatec.com.br/
[paginate]: https://github.com/mislav/will_paginate
[psql]: https://en.wikipedia.org/wiki/PostgreSQL
[heroku]: https://en.wikipedia.org/wiki/Heroku
[jquery]: https://en.wikipedia.org/wiki/JQuery
[bootstrap]: https://en.wikipedia.org/wiki/Bootstrap_%28front-end_framework%29
[fontawesome]: http://fontawesome.io/
[d3]: https://en.wikipedia.org/wiki/D3.js
[pivotal-tracker]: http://pivotal.io/labs
[demo]: http://meter-man.herokuapp.com/
[install]: notes_on_dev_env.md
[gh-bjorn]: https://github.com/Bjornkjohnson
[gh-ian]: https://github.com/ian-s-mcb
[gh-simon]: https://github.com/Santos94
[gh-steven]: https://github.com/Dante1226
